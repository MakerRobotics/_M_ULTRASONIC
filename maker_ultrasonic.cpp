/*
 * Library: maker_ultrasonic.h
 *
 * Organization: MakerRobotics
 * Autors: Katarina Stankovic,
           Milan Gigic,
		   Maksim Goranov Jovovic.
 * 
 * Date: 10.11.2017.
 * Test: Arduino UNO
 * 
 * Note:
 * If there are more sensors, user needs to setUltrasonic every time before usage of other functions
 */
#include "Arduino.h"
#include "maker_ultrasonic.h"
#include "stdint.h"

uint8_t _trig;
uint8_t _echo;

void maker_setUltrasonic(uint8_t pinTrig, uint8_t pinEcho)
{
	_trig=pinTrig;
	_echo=pinEcho;
	pinMode(pinTrig,1);
	pinMode(pinEcho,0);
}

static double _returnTime()
{
	digitalWrite(_trig, 0);
	delayMicroseconds(5);
	digitalWrite(_trig, 1);
	delayMicroseconds(5);
	digitalWrite(_trig, 0);
	return pulseIn(_echo,1)/2;
}

double maker_ultrasonicDistance_cm()
{
	return _returnTime()/29;
}

double maker_ultrasonicDistance_mm()
{
	return maker_ultrasonicDistance_cm()*10;
}

double maker_ultrasonicDistance_m()
{
	return maker_ultrasonicDistance_cm()/100;
}

double maker_ultrasonicDistance_inch()
{
	return maker_ultrasonicDistance_cm()/2.54;
}
