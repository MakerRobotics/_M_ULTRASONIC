#ifndef _MAKER_ULTRASONIC_
#define _MAKER_ULTRASONIC_

extern uint8_t _trig;
extern uint8_t _echo;

/*
 * Arguments: pinTrig	pin attached to Trig 
              pinEcho	pin attached to Echo   
 *
 * Note: Sets pin modes; Trig->OUTPUT, Echo->INPUT.
 *
 */
void maker_setUltrasonic(uint8_t pinTrig, uint8_t pinEcho);

/*
 * Note: Returns distance of object in cm
 */
double maker_ultrasonicDistance_cm();

/*
 * Note: Returns distance of object in mm
 */ 
double maker_ultrasonicDistance_mm();

/*
 * Note: Returns distance of object in m
 */
double maker_ultrasonicDistance_m();

/*
 * Note: Returns distance of object in inch
 */
double maker_ultrasonicDistance_inch();

#endif
